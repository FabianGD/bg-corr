# Baseline correction for Raman data

The scripts presented here conduct a baseline correction for Raman and Resonance Raman spectra based on an algorithm developed by [Zhang _et al._, 2009](https://doi.org/10.1002/jrs.2500), as maintained as [`baselineWavelet`](https://github.com/zmzhang/baselineWavelet) on GitHub.

## Installation

Personally, I'd recommend installing [Windows Subsystem for Linux (WSL)](https://docs.microsoft.com/en-us/windows/wsl/install-win10#manual-installation-steps) on Windows 10 machines. Here, make sure to select WSL version 2 which typically works better. You can use plain Windows, but that will require you to install `libgit2` (and probably `harfbuzz`) from source.

### Pre-installation

The following libraries need to be installed on your machine:

- `libgit2` (can be found [here on GitHub](https://github.com/libgit2/libgit2)).
- `harfbuzz` (can be found [here on GitHub](https://github.com/harfbuzz/harfbuzz)).

In WSL, the libraries can be installed (assuming Debian or Ubuntu as WSL distributions) via:

```bash
sudo apt-get update
sudo apt-get install libgit2 harfbuzz
```

### Installing R

On a Windows machine, install the [R](https://www.r-project.org/) interpreter by following the installation guidelines given [here](https://cloud.r-project.org/). On WSL, installing R is as easy as running

```bash
sudo apt-get update && sudo apt-get install R
```

### Installing the dependencies of the baseline correction script

The scripts come with a script `install.R`, that can be used to install all the necessary packages including the `baselineWavelet` library. Installation of the packages might take a while, depending on your machine and operating system. In your command line (Ubuntu WSL or Powershell), run:

```bash
Rscript install.R
```

## Usage

Once everything has installed without errors, you're ready to rock. All you need to run is:

```bash
Rscript do_BGcorr.R -f <data-folder> -o <output-folder>
```

where `<data-folder>` is the relative or absolute path to your data and `<output-folder>` (which is optional) is the folder name where you want the baseline-corrected data to appear. The latter one defaults to a folder called `output` in the raw data folder. You can get the help text by running:

```bash
Rscript do_BGcorr.R --help
```

To test the functionality I provide a sample data file in the `data` folder. Feel free to use it for testing purposes.

## In case of questions

In case you have any sort of questions or comments regarding this set of scripts, feel free to contact [me](mailto:fabian.droege@uni-jena.de) or open a new issue in this Git repo.
